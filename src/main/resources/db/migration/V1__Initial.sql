CREATE TABLE categories (
  "id"         VARCHAR PRIMARY KEY,
  "name"       VARCHAR   NOT NULL,
  "is_active"  BOOLEAN   NOT NULL,
  "created_at" TIMESTAMP NOT NULL
);

CREATE TABLE dishes  (
  "id" VARCHAR PRIMARY KEY,
  "title" VARCHAR NOT NULL,
  "description" VARCHAR,
  "price" NUMERIC NOT NULL,
  "category_id" VARCHAR NOT NULL,
  "is_available" BOOLEAN NOT NULL,
  "created_at" TIMESTAMP NOT NULL,
  FOREIGN KEY ("category_id") REFERENCES "categories" (id)
);

CREATE TABLE "order" (
  "id" VARCHAR PRIMARY KEY,
  "created_at" TIMESTAMP NOT NULL,
  "is_confirmed" BOOLEAN NOT NULL
);

CREATE TABLE order_entry(
  "id" VARCHAR PRIMARY KEY,
  "dish_id" VARCHAR NOT NULL ,
  "order_id" VARCHAR NOT NULL,
  "dish_price" NUMERIC NOT NULL,
  "dish_name" VARCHAR NOT NULL,
  "created_at" TIMESTAMP NOT NULL,
  "quantity" INTEGER NOT NULL,
  FOREIGN KEY ("order_id") REFERENCES "order"(id)
);

CREATE TABLE delivery_info(
  "id" VARCHAR PRIMARY KEY,
  "order_id" VARCHAR NOT NULL,
  "name" VARCHAR NOT NULL,
  "address" VARCHAR NOT NULL,
  "email_address" VARCHAR NOT NULL,
  "phone_number" VARCHAR NOT NULL,
  FOREIGN KEY ("order_id") REFERENCES "order"(id)
);

INSERT INTO categories VALUES ('1', 'sushi', true, CURRENT_TIMESTAMP);
INSERT INTO dishes VALUES ('1', 'sashimi', 'soome kind of sushi', 2.5, 1, true, CURRENT_TIMESTAMP);
INSERT INTO dishes VALUES ('2', 'sushi', 'some kind of sushi', 5, 1, true, CURRENT_TIMESTAMP);


