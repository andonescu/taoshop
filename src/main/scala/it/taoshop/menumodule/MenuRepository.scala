package it.taoshop.menumodule

import java.sql.Timestamp

import it.taoshop.common.domain.BasicData
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile
import slick.lifted.ProvenShape

/**
  * Created by d.stratulat on 11/29/16.
  */

case class DishData(id: String,
                    title: String,
                    description: String,
                    price: Double,
                    categoryId: String,
                    isAvailable: Boolean,
                    createdAt: Timestamp) extends BasicData

case class CategoryData(id: String, name: String, isActive: Boolean, createdAt: Timestamp) extends BasicData

class MenuRepository(val driver: JdbcProfile)(implicit config: DatabaseConfig[JdbcProfile]) {

  import driver.api._

  implicit val exc = scala.concurrent.ExecutionContext.global
  val name = "dishes"

  class DishTable(tag: Tag) extends Table[DishData](tag, name) {

    def id: Rep[String] = column[String]("id", O.PrimaryKey)

    def title: Rep[String] = column[String]("title")

    def description: Rep[String] = column[String]("description")

    def price: Rep[Double] = column[Double]("price")

    def categoryId: Rep[String] = column[String]("category_id")

    def isAvailable: Rep[Boolean] = column[Boolean]("is_available")

    def createdAt: Rep[Timestamp] = column[Timestamp]("created_at")

    def categoryFk = foreignKey("CATEGORY_FK", categoryId, categories)(_.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)

    override def * : ProvenShape[DishData] =
      (id, title, description, price, categoryId, isAvailable, createdAt) <> (DishData.tupled, DishData.unapply)
  }

  class CategoryTable(tag: Tag) extends Table[CategoryData](tag, "categories") {
    def id = column[String]("id", O.PrimaryKey)

    def name = column[String]("name")

    def isActive = column[Boolean]("is_active")

    def createdAt: Rep[Timestamp] = column[Timestamp]("created_at")

    override def * : ProvenShape[CategoryData] = (id, name, isActive, createdAt) <> (CategoryData.tupled, CategoryData.unapply)
  }

  private[this] val dishes: TableQuery[DishTable] = TableQuery[DishTable]
  private[this] val categories: TableQuery[CategoryTable] = TableQuery[CategoryTable]

  def insertDish(dish: DishData) =
    config.db.run(dishes += dish)

  def allActiveDishes() =
    config.db.run(dishes.filter(_.isAvailable).result)

  def getDishById(id: String) =
    config.db.run(dishes.filter(_.id === id).result.headOption)

  def findAllActiveCategories() =
    config.db.run(categories.filter(_.isActive).result)

  def insertCategory(category: CategoryData) =
    config.db.run(categories += category)

}

