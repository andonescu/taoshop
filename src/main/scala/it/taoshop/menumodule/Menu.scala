package it.taoshop.menumodule

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import spray.json._

import scala.concurrent.ExecutionContext

case class Category(dishes: Seq[Dish], id: String, name: String)

case class Dish(id: String, title: String, description: String, price: Double, categoryId: String)

trait MenuFormats extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val dishFormat: RootJsonFormat[Dish] = DefaultJsonProtocol.jsonFormat5(Dish)
  implicit val categoryFormat: RootJsonFormat[Category] = DefaultJsonProtocol.jsonFormat3(Category)
  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
}


class Menu(menuRepo: MenuRepository) extends MenuFormats {
  val routes: Route = pathPrefix("menu") {
    path("dishes") {
      get {
        val menuData = for {
          dishes <- menuRepo.allActiveDishes()
          categoriesData <- menuRepo.findAllActiveCategories()
        } yield {

          val groupedDishes: Map[String, Seq[Dish]] =
            dishes.map(d => Dish(d.id, d.title, d.description, d.price, d.categoryId)).groupBy(_.categoryId)
            categoriesData.map(cat => Category(groupedDishes.getOrElse(cat.id, Seq()), cat.id, cat.name))
        }

        complete(menuData)
      }
    }
  }
}




