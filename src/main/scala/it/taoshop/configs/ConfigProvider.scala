package it.taoshop.configs

import com.typesafe.config.ConfigFactory

/**
  * Created by d.stratulat on 1/10/17.
  */
private[configs] object ConfigProvider {
    lazy val configuration = ConfigFactory.load()
}
