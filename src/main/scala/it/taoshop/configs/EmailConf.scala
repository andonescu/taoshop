package it.taoshop.configs

/**
  * Created by d.stratulat on 2/6/17.
  */
trait EmailConf {
  protected val protocol = ConfigProvider.configuration.getString("email.protocol")
  protected val senderEmail = ConfigProvider.configuration.getString("email.sender")
  protected val senderEmailPassword = ConfigProvider.configuration.getString("email.password")
  protected val port = ConfigProvider.configuration.getInt("email.port")
}
