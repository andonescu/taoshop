package it.taoshop.configs

/**
  * Created by d.stratulat on 2/14/17.
  */
trait HttpConfigs {
  val baseURL: String = ConfigProvider.configuration.getString("http.base_url")
}
