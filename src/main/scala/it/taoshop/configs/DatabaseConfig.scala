package it.taoshop.configs

/**
  * Created by d.stratulat on 1/10/17.
  */
trait DatabaseConfig {
  private val dbConfig = ConfigProvider.configuration.getConfig("database.db")
  private[configs] lazy val jdbcUrl: String = dbConfig.getString("url")
  private[configs] lazy val username: String = dbConfig.getString("user")
  private[configs] lazy val password: String = dbConfig.getString("password")
}
