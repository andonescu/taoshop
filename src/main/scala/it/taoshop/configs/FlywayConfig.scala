package it.taoshop.configs

import org.flywaydb.core.Flyway

/**
  * Created by d.stratulat on 1/10/17.
  */
class FlywayConfig () {
  self:DatabaseConfig =>

  private[this] val flywayInstance = new Flyway


  def migrateSchema: Int = {
    flywayInstance.setDataSource(self.jdbcUrl, self.username, self.password)
    flywayInstance.setBaselineOnMigrate(true)
    flywayInstance.migrate()
  }

  def cleanSchema: Unit = {
    flywayInstance.clean()
    flywayInstance.migrate()
  }
}
