package it.taoshop

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import it.taoshop.common.JdbcConnectionProvider
import it.taoshop.configs.{DatabaseConfig, FlywayConfig, HttpConfigs}
import it.taoshop.menumodule.{Dish, Menu, MenuRepository}
import it.taoshop.notification.Emailer
import it.taoshop.ordermodule.{OrderAPI, OrderRepository, OrderService}
import org.slf4j.LoggerFactory

import scala.concurrent.Await
import scala.concurrent.duration._


/**
  * Created by d.stratulat on 11/28/16.
  */
object Main extends App with JdbcConnectionProvider with HttpConfigs {
  implicit val system = ActorSystem("taoshop-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  implicit val dbConfig = connection

  private val logger = LoggerFactory.getLogger(this.getClass)

  val menuRepository = new MenuRepository(profile)
  val orderService = new OrderService(
    new OrderRepository(profile),
    (dishID) => {
      Await.result(menuRepository.getDishById(dishID), 10 seconds)
        .map(dt => Dish(dt.id, dt.title, dt.description, dt.price, dt.categoryId)).get
    }
  )

  val emailer = new Emailer

  val flywayConfig = new FlywayConfig() with DatabaseConfig
  flywayConfig.migrateSchema

  val menu = new Menu(menuRepository)
  val orderAPI = new OrderAPI(orderService, emailer)

  val route = pathPrefix("api") {
    menu.routes ~ orderAPI.routes
  }

  val binding = Http().bindAndHandle(route, "localhost", 3000)
  binding.onFailure {
    case ex => println(s"error $ex")
  }

  logger.info("App started")
}

