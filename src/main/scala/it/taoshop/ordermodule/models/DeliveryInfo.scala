package it.taoshop.ordermodule.models

/**
  * Created by d.stratulat on 1/3/17.
  */
case class DeliveryInfo(address:String, name:String, email:String, phoneNumber:String)
