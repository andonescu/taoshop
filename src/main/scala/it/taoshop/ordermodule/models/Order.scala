package it.taoshop.ordermodule.models

/**
  * Created by d.stratulat on 1/3/17.
  */
case class Order(orderId:String, entries:Seq[OrderEntry], deliveryInfo: Option[DeliveryInfo])
