package it.taoshop.ordermodule.models

import it.taoshop.menumodule.Dish

/**
  * Created by d.stratulat on 1/3/17.
  */
case class OrderEntry (id:Option[String], dish:Dish, quantity : Int){
  def map[R](f : OrderEntry => R): R = f(this)
}

//TODO: Order entry id is irrelevant, it can be removed from dto