package it.taoshop.ordermodule.models

/**
  * Created by d.stratulat on 1/3/17.
  */
case class Checkout(deliveryInfo: DeliveryInfo, orderId:String)