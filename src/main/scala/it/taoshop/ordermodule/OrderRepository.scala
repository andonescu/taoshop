package it.taoshop.ordermodule

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import it.taoshop.common.domain.BasicData
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import slick.lifted.{ForeignKeyQuery, ProvenShape}

import scala.concurrent.{ExecutionContextExecutor, Future}

/**
  * Created by d.stratulat on 12/29/16.
  */

case class OrderData(id: String, createdAt: Timestamp, isConfirmed: Boolean = false) extends BasicData

case class OrderEntryData(id: String, dishId: String, orderId: String, dishPrice: Double, quantity: Int, dishName: String, createdAt: Timestamp) extends BasicData

case class DeliveryInfoData(id: String, orderId: String, name: String, address: String, emailAddress: String, phoneNumber: String)

class OrderRepository(val driver: JdbcProfile)(implicit config: DatabaseConfig[JdbcProfile]) {

  import driver.api._

  implicit val exc: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global
  private val db = config.db

  class OrderTable(tag: Tag) extends Table[OrderData](tag, "order") {
    def id: Rep[String] = column[String]("id", O.PrimaryKey)

    def createdAt: Rep[Timestamp] = column[Timestamp]("created_at")

    def isConfirmed: Rep[Boolean] = column[Boolean]("is_confirmed")

    override def * : ProvenShape[OrderData] = (id, createdAt, isConfirmed) <> (OrderData.tupled, OrderData.unapply)
  }

  class OrderEntryTable(tag: Tag) extends Table[OrderEntryData](tag, "order_entry") {
    def id: Rep[String] = column[String]("id", O.PrimaryKey)

    def dishId: Rep[String] = column[String]("dish_id")

    def orderId: Rep[String] = column[String]("order_id")

    def dishPrice: Rep[Double] = column[Double]("dish_price")

    def quantity: Rep[Int] = column[Int]("quantity")

    def dishName: Rep[String] = column[String]("dish_name")

    def createdAt: Rep[Timestamp] = column[Timestamp]("created_at")

    def orderFk: ForeignKeyQuery[OrderTable, OrderData] = foreignKey("ORDER_FK", orderId, orders)(_.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)

    override def * : ProvenShape[OrderEntryData] = (id, dishId, orderId, dishPrice, quantity, dishName, createdAt) <> (OrderEntryData.tupled, OrderEntryData.unapply)
  }

  class DeliveryInfoTable(tag: Tag) extends Table[DeliveryInfoData](tag, "delivery_info") {
    def id: Rep[String] = column[String]("id", O.PrimaryKey)

    def orderId: Rep[String] = column[String]("order_id")

    def name: Rep[String] = column[String]("name")

    def address: Rep[String] = column[String]("address")

    def emailAddress: Rep[String] = column[String]("email_address")

    def phoneNumber: Rep[String] = column[String]("phone_number")

    def orderFK: ForeignKeyQuery[OrderTable, OrderData] = foreignKey("ORDER_FK", orderId, orders)(_.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)

    override def * : ProvenShape[DeliveryInfoData] = (id, orderId, name, address, emailAddress, phoneNumber) <> (DeliveryInfoData.tupled, DeliveryInfoData.unapply)
  }

  private[this] val entries: TableQuery[OrderEntryTable] = TableQuery[OrderEntryTable]
  private[this] val orders: TableQuery[OrderTable] = TableQuery[OrderTable]
  private[this] val deliveryInfos: TableQuery[DeliveryInfoTable] = TableQuery[DeliveryInfoTable]

  def openANewOrder(): Future[String] = {
    val insertQuery = orders returning orders.map(_.id) into ((o, id) => o.copy(id = id))
    val action = insertQuery += OrderData(UUID.randomUUID().toString, Timestamp.valueOf(LocalDateTime.now()))
    db.run(action).map(r => r.id)
  }

  def getOrderById(id: String): Future[Option[OrderData]] = db.run {
    (for (order <- orders if order.id === id) yield order).result.headOption
  }

  def addEntry(orderEntry: OrderEntryData): Future[Int] = db.run {
    entries.insertOrUpdate(orderEntry)
  }

  def getAllEntriesByOrderId(orderId: String): Future[Seq[OrderEntryData]] = db.run {
    (for (entry <- entries if entry.orderId === orderId) yield entry).result
  }

  def getOrderEntryById(orderEntryId: String): Future[Option[OrderEntryData]] = db.run {
    (for (entry <- entries if entry.id === orderEntryId) yield entry).result.headOption
  }

  def updateOrderIsConfirmed(orderId: String, isConfirmed: Boolean): Future[Int] = db.run {
    (for (order <- orders if order.id === orderId) yield order.isConfirmed).update(isConfirmed)
  }

  def updateDeliveryInfo(deliveryInfoData: DeliveryInfoData): Future[Int] = db.run {
    deliveryInfos.insertOrUpdate(deliveryInfoData)
  }

  def getDeliveryInfoByOrderId(orderId: String): Future[Option[DeliveryInfoData]] = db.run {
    (for (info <- deliveryInfos if info.orderId === orderId) yield info).result.headOption
  }
}
