package it.taoshop.ordermodule.validators
import cats.Apply
import cats.data.ValidatedNel
import it.taoshop.ordermodule.models.DeliveryInfo

/**
  * Created by d.stratulat on 1/24/17.
  */



object DeliveryInfoValidator {
  def validate(deliveryInfo: DeliveryInfo) = {
    Apply[ValidatedNel[ErrorMessage, ?]].map4(
      Validators.nonEmpty("address", deliveryInfo.address).toValidatedNel,
      Validators.email(deliveryInfo.email).toValidatedNel,
      Validators.nonEmpty("name",deliveryInfo.name).toValidatedNel,
      Validators.nonEmpty("phoneNumber", deliveryInfo.phoneNumber).toValidatedNel
    ){
      case (address, email, name, phoneNumber) => DeliveryInfo(address, name, email, phoneNumber)
    }
  }
}
