package it.taoshop.ordermodule.validators

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}


/**
  * Created by d.stratulat on 1/24/17.
  */


sealed trait ErrorMessage {
  val message: String
  val key:String
}

case class InvalidEmailError(message: String = "Invalid email address", key:String = "emailAddress") extends ErrorMessage

case class NonEmptyError(message: String = "Value should not be empty", key:String) extends ErrorMessage

private[validators] object Validators {
  private val emailRegex = """^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""".r

  def email(email: String): Validated[ErrorMessage, String] = email match {
    case e if e.trim.isEmpty => Invalid(InvalidEmailError())
    case e if emailRegex.findFirstMatchIn(e).isDefined => Valid(e)
    case _=> Invalid(InvalidEmailError())
  }

  def nonEmpty(key:String, value: String): Validated[ErrorMessage, String] = value match {
    case v if v.trim.isEmpty => Invalid(NonEmptyError(key=key))
    case v => Valid(v)
  }

}