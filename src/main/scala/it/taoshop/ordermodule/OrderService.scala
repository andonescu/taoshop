package it.taoshop.ordermodule

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import it.taoshop.menumodule.Dish
import it.taoshop.ordermodule.models.{DeliveryInfo, Order, OrderEntry}

import scala.concurrent.{ExecutionContext, Future}

sealed trait ServiceResponse

case class Updated[T](value: T) extends ServiceResponse

case object Failed extends ServiceResponse


/**
  * Created by d.stratulat on 1/3/17.
  */
class OrderService(orderRepository: OrderRepository, dishProvider: String => Dish)
                  (implicit exc: ExecutionContext) {

  private def transFormToOrderEntry(orderEntryData: OrderEntryData): OrderEntry =
    OrderEntry(
      Some(orderEntryData.id),
      dishProvider(orderEntryData.dishId),
      orderEntryData.quantity
    )

  def createOrder() =
    orderRepository.openANewOrder()

  def addEntry(orderEntry: OrderEntry, orderId: String): Future[Int] = {
    val timestamp = Timestamp.valueOf(LocalDateTime.now())

    val orderEntryData = orderEntry.map {
      oe =>
        OrderEntryData(
          UUID.randomUUID().toString,
          dishId = oe.dish.id,
          orderId = orderId,
          dishPrice = oe.dish.price,
          quantity = oe.quantity,
          dishName = oe.dish.title,
          createdAt = timestamp)
    }

    orderRepository.addEntry(orderEntryData)
  }

  def saveDeliveryInfo(orderId: String, deliveryInfo: DeliveryInfo): Future[Int] = {
    orderRepository.updateDeliveryInfo(
      DeliveryInfoData(
        UUID.randomUUID().toString,
        orderId = orderId,
        name = deliveryInfo.name,
        address = deliveryInfo.address,
        emailAddress = deliveryInfo.email,
        phoneNumber = deliveryInfo.phoneNumber
      ))
  }

  def confirmOrder(orderId: String): Future[Int] = {
    orderRepository.updateOrderIsConfirmed(orderId, isConfirmed = true)
  }

  def getOrder(orderId: String): Future[Option[Order]] =
    (
      for {
        orderDataOpt <- orderRepository.getOrderById(orderId)
        orderDataEntries <- orderRepository.getAllEntriesByOrderId(orderId)
        deliverInfoOpt <- orderRepository.getDeliveryInfoByOrderId(orderId)
      } yield (orderDataOpt, orderDataEntries, deliverInfoOpt)
    ).map {

      case (Some(order), entries, Some(deliveryInfo)) =>
        val deliverData = DeliveryInfo(deliveryInfo.address, deliveryInfo.name, deliveryInfo.emailAddress, deliveryInfo.phoneNumber)
        Some(Order(order.id, entries.map(transFormToOrderEntry), Some(deliverData)))

      case (Some(order), entries, None) =>
        Some(Order(order.id, entries.map(transFormToOrderEntry), None))

      case _ => None
    }

  def getDeliveryInfoByOrderId(orderId: String): Future[Option[DeliveryInfoData]] = {
    orderRepository.getDeliveryInfoByOrderId(orderId)
  }
}

