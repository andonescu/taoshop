package it.taoshop.ordermodule

/**
  * Created by d.stratulat on 12/16/16.
  */

import javax.mail.internet.InternetAddress

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import cats.data.OptionT
import cats.data.Validated.{Invalid, Valid}
import cats.implicits._
import it.taoshop.configs.HttpConfigs
import it.taoshop.menumodule.Dish
import it.taoshop.notification.{Emailer, OrderEmailDTO}
import it.taoshop.ordermodule.models.{Checkout, DeliveryInfo, Order, OrderEntry}
import it.taoshop.ordermodule.validators.DeliveryInfoValidator
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

import scala.concurrent.ExecutionContext

trait OrderFormat extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val dish:RootJsonFormat[Dish] = jsonFormat5(Dish)
  implicit val orderEntryFormat: RootJsonFormat[OrderEntry] = jsonFormat3(OrderEntry)
  implicit val deliveryInfoFormat: RootJsonFormat[DeliveryInfo] = jsonFormat4(DeliveryInfo)
  implicit val checkoutFormat: RootJsonFormat[Checkout] = jsonFormat2(Checkout)
  implicit val orderFormat: RootJsonFormat[Order] = jsonFormat3(Order)
  implicit val errorMessageFormat: RootJsonFormat[ErrorMessageData] = jsonFormat2(ErrorMessageData)
  implicit val createOrderResponseFormat : RootJsonFormat[CreateOrderResponse] = jsonFormat1(CreateOrderResponse)
}

case class ErrorMessageData(key: String, Message: String)
case class CreateOrderResponse(orderId:String)

class OrderAPI(orderService: OrderService, emailer: Emailer)(implicit exc: ExecutionContext) extends OrderFormat with HttpConfigs {

  private def executeWhenOrderWithDeliveryInfo(orderId: String)(f: (Order) => Unit) =
    OptionT(orderService.getOrder(orderId)).filter(_.deliveryInfo.isDefined).map(f)

  private def handleCheckoutRoute(orderId: String) = (path("checkout") & put) {
    complete {
      executeWhenOrderWithDeliveryInfo(orderId) {
        order =>
          val confirmationURL = baseURL + "/order/confirm/" + orderId
          emailer.sendConfirmationRequestEmail(
            OrderEmailDTO(Some(confirmationURL), order, new InternetAddress(order.deliveryInfo.get.email)))
      }
      OK
    }
  }

  private def saveDeliveryInfo(orderId: String) = (path("deliveryinfo") & post) {
    entity(as[DeliveryInfo]) {
      deliveryInfo =>
        DeliveryInfoValidator.validate(deliveryInfo) match {
          case Valid(_) =>
            complete(orderService.saveDeliveryInfo(orderId, deliveryInfo).map{
              case 0 => InternalServerError
              case 1 => OK
            })
          case Invalid(errorList) =>
            complete(errorList.map(ms => ErrorMessageData(ms.key, ms.message)).toList)
          case _ => complete(NotFound)
        }
    }
  }

  private def handleAddOrRemoveDish(orderId: String) = path("dish") {
    post {
      entity(as[OrderEntry]) {
        orderEntry =>
          complete(orderService.addEntry(orderEntry, orderId).map(v => if (v > 0) OK else NotFound))
      }
    }
  }

  private val createOrder = (path("create") & get) {
    complete(orderService.createOrder().map(o => CreateOrderResponse(o)))
  }

  private val confirmOrder = (path("confirm" / Segment) & post) {
    orderId =>
      val response = for {
        v <- orderService.confirmOrder(orderId)
      } yield {
        if (v > 0) {
          executeWhenOrderWithDeliveryInfo(orderId) {
            order =>
              emailer.sendConfirmationRequestEmail(
                OrderEmailDTO(None, order, new InternetAddress(order.deliveryInfo.get.email)))
          }
          OK
        } else NotFound
      }
      complete(response)
  }

  val routes: Route = pathPrefix("order") {
    createOrder ~
      confirmOrder ~
      pathPrefix(Segment) {
        orderId => {
          get {
            onSuccess(orderService.getOrder(orderId)) {
              case order@Some(o) => complete(order)
              case None => complete(NotFound)
            }
          } ~
            saveDeliveryInfo(orderId) ~
            handleAddOrRemoveDish(orderId) ~
            handleCheckoutRoute(orderId)
        }
      }
  }
}

