package it.taoshop.common

import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

/**
  * Created by d.stratulat on 12/19/16.
  */
trait DatabaseProvider[T <: JdbcProfile] {
 def connection: DatabaseConfig[T]
 def profile : JdbcProfile
}
