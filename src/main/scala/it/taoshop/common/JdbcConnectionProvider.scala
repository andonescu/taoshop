package it.taoshop.common

import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

/**
  * Created by d.stratulat on 12/19/16.
  */
trait JdbcConnectionProvider extends DatabaseProvider[JdbcProfile] {
  override def connection: DatabaseConfig[JdbcProfile] = DatabaseConfig.forConfig("database")
  override def profile = slick.driver.PostgresDriver
}
