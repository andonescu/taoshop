package it.taoshop.common.domain

import java.sql.Timestamp

trait BasicData {
  def id: String

  def createdAt: Timestamp
}