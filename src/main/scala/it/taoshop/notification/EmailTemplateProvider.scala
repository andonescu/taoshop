package it.taoshop.notification

import java.io.{File, StringWriter}

import freemarker.template.Configuration
import it.taoshop.notification.Templates.Template

/**
  * Created by d.stratulat on 2/6/17.
  */

object Templates {
  sealed trait Template{
    val name:String
  }

  case class OrderConfirmationEmail(name :String= "orderConfirmationEmail.ftl") extends Template
  case class KitchenNotificationEmail(name :String= "kitchenNotificationEmail.ftl") extends Template
}
trait EmailTemplateProvider {
  private var templateConf: Configuration = {
    var newConfg = new Configuration(Configuration.VERSION_2_3_23)
    var classLoader = getClass.getClassLoader
    newConfg.setDirectoryForTemplateLoading(new File(classLoader.getResource("templates/email").getFile))
    newConfg.setDefaultEncoding("UTF-8")
    newConfg.setLogTemplateExceptions(false)
    newConfg
  }

  def getTemplate[T](template:Template, dto:T):String = {
    val writer = new StringWriter()
    templateConf.getTemplate(template.name).process(dto,writer)
    val generatedHTML = writer.toString
    writer.close()
    generatedHTML
  }
}
