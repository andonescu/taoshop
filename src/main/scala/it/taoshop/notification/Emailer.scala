package it.taoshop.notification

import javax.mail.internet.InternetAddress

import com.github.jurajburian.mailer._
import it.taoshop.configs.EmailConf
import it.taoshop.ordermodule.models.Order

/**
  * Created by d.stratulat on 2/6/17.
  */

case class OrderEmailDTO(confirmationURL :Option[String], order:Order, toEmail : InternetAddress)

class Emailer extends EmailTemplateProvider with EmailConf {

  private val session = (SmtpAddress(protocol, port) :: SessionFactory()).session(Some(senderEmail -> senderEmailPassword))
  private val kitchenEmail = ""

  def sendConfirmationRequestEmail(confirmationEmailDTO: OrderEmailDTO): Unit = {
    val content = new Content().html(getTemplate(Templates.OrderConfirmationEmail(), confirmationEmailDTO))
    val msg = Message(
      from = new InternetAddress(senderEmail),
      subject ="Confirm TaoSushi order",
      content = content,
      to = Seq(confirmationEmailDTO.toEmail)
    )
    Mailer(session).send(msg)
  }

  def sendNotification(dto:OrderEmailDTO): Unit ={
    val content  = new Content().html(getTemplate(Templates.KitchenNotificationEmail(), dto))
    val msg = Message(
      from = new InternetAddress(senderEmail),
      subject ="Notification tao sushi ",
      content = content,
      to = Seq(new InternetAddress(kitchenEmail))
    )
  }

}
