package util

import it.taoshop.configs.{DatabaseConfig, FlywayConfig}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

/**
  * Created by d.stratulat on 12/11/16.
  */
trait DatabaseConnection extends FlatSpec with Matchers with BeforeAndAfterAll {
  val flyway = new FlywayConfig() with DatabaseConfig

  override def afterAll() = {
    flyway.cleanSchema
  }

  override def beforeAll() = {
    flyway.migrateSchema
  }

}
