package util

import it.taoshop.common.DatabaseProvider
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

/**
  * Created by d.stratulat on 12/19/16.
  */
trait H2ConfigProvider extends DatabaseProvider[JdbcProfile]{
  override def connection = DatabaseConfig.forConfig[JdbcProfile]("database")

  override def profile = slick.driver.PostgresDriver
}
