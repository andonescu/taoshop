package it.taoshop.configs

import org.scalatest.{BeforeAndAfter, FlatSpec}

/**
  * Created by d.stratulat on 1/16/17.
  */
class FlywayConfigTest extends FlatSpec with BeforeAndAfter{
  val flyWayConf = new FlywayConfig() with  DatabaseConfig

  "Migration" should "not fail" in{
    flyWayConf.migrateSchema
  }

  after{
    flyWayConf.cleanSchema
  }

}
