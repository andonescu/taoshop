package it.taoshop.ordermodule.validators

import cats.data.Validated.{Invalid, Valid}
import org.scalatest.{Matchers, WordSpec}

/**
  * Created by d.stratulat on 2/1/17.
  */
class ValidatorsSpec extends WordSpec with Matchers {

  "Email validation" should {
    "be valid" in {
      val emailAddress = "valid@email.com"
      Validators.email(emailAddress) shouldBe Valid(emailAddress)
    }
    "not be valid" in{
      Validators.email("not_a_valid_email") shouldBe Invalid(InvalidEmailError())
    }

    "return invalid if email is empty" in {
      Validators.email("") shouldBe Invalid(InvalidEmailError())
    }
  }

  "NonEmpty" should{
    "return valid" in {
      Validators.nonEmpty("Address","Ana Ipatescu") shouldBe Valid("Ana Ipatescu")
    }

    "return invalid" in {
      Validators.nonEmpty("name","") shouldBe Invalid(NonEmptyError(key="name"))
    }
  }

}
