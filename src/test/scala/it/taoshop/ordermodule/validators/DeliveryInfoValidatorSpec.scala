package it.taoshop.ordermodule.validators

import cats.data.Validated.Invalid
import it.taoshop.ordermodule.models.DeliveryInfo
import org.scalatest.{Matchers, WordSpec}

/**
  * Created by d.stratulat on 2/1/17.
  */
class DeliveryInfoValidatorSpec extends WordSpec with Matchers{
  "Should return a list of validation errors" in {
    val result = DeliveryInfoValidator.validate(DeliveryInfo("", "", "not_an_email", "012"))
    result shouldBe a [Invalid[_]]
    val errors = result.valueOr(messages => messages.toList)
    errors shouldBe List(NonEmptyError(key = "address"), InvalidEmailError(), NonEmptyError(key ="name"))
  }

  "Should be valid" in {
    DeliveryInfoValidator
      .validate(DeliveryInfo("Ana Ipatescu", "Dragos Stratulat", "dragos@gmail.com","023232123")).isValid shouldBe true
  }
}
