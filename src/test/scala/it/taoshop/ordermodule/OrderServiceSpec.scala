package it.taoshop.ordermodule

import java.sql.Timestamp
import java.time.LocalDateTime

import it.taoshop.menumodule.Dish
import it.taoshop.ordermodule.models.OrderEntry
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.concurrent.Eventually
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent._
import scala.concurrent.duration._

/**
  * Created by d.stratulat on 1/4/17.
  */
class OrderServiceSpec extends WordSpec with MockitoSugar with Matchers with Eventually {
  val orderRepository: OrderRepository = mock[OrderRepository]
  implicit val exc: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global
  val subject = new OrderService(orderRepository, (dishID) => Dish("dish_Id", "some title","some desc", 2.00, "catg_id"))


  "Should return the created order " in {
    when(orderRepository.openANewOrder()).thenReturn(Future.successful("1"))
    Await.result(subject.createOrder(), 2 seconds) shouldBe "1"
  }

  "Should transform order entry into order entry data when adding a new entry" in {
    val captor: ArgumentCaptor[OrderEntryData] = ArgumentCaptor.forClass(classOf[OrderEntryData])
    subject.addEntry(OrderEntry(None,Dish("dish_id","","",2.00, ""), 2), "orderID")
    verify(orderRepository, atLeastOnce()).addEntry(captor.capture())
    captor.getValue.orderId shouldEqual "orderID"
  }

  "GetOrder" should {
    "return None if could not find order" in {
      when(orderRepository.getOrderById(anyString())).thenReturn(Future.successful(None))
      when(orderRepository.getAllEntriesByOrderId(anyString())).thenReturn(Future.successful(Seq()))
      when(orderRepository.getDeliveryInfoByOrderId(anyString())).thenReturn(Future.successful(None))
      Await.result(subject.getOrder("someId"), 2 seconds) shouldBe None
    }
    "return a Order with empty entries if there aren't any entries" in {
      when(orderRepository.getOrderById(anyString()))
        .thenReturn(Future.successful(Some(OrderData("orderId", Timestamp.valueOf(LocalDateTime.now())))))
      when(orderRepository.getAllEntriesByOrderId(anyString())).thenReturn(Future.successful(Seq()))
      when(orderRepository.getDeliveryInfoByOrderId(anyString())).thenReturn(Future.successful(None))
      val result = Await.result(subject.getOrder("someId"), 2 seconds)
      result.get.entries shouldBe Some(Nil)
    }
  }
}

