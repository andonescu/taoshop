package it.taoshop.ordermodule

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import it.taoshop.menumodule.Dish
import it.taoshop.notification.Emailer
import it.taoshop.ordermodule.models.{Order, OrderEntry}
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.{ExecutionContextExecutor, Future}


/**
  * Created by d.stratulat on 12/30/16.
  */
class OrderSpec extends WordSpec with ScalatestRouteTest with Matchers with OrderFormat with MockitoSugar {

  implicit val exc: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global
  val orderService: OrderService = mock[OrderService]
  val emailer: Emailer = mock[Emailer]
  when(orderService.createOrder()).thenReturn(Future.successful("12wdsadq"))
  val ordersApi = new OrderAPI(orderService, emailer)


  "Create should return order id" in {
    val expectedOrderId = "order_id"
    when(orderService.createOrder()).thenReturn(Future.successful(expectedOrderId))
    Get("/order/create") ~> ordersApi.routes ~> check {
      status shouldBe OK
      responseAs[CreateOrderResponse].orderId shouldBe expectedOrderId
    }
  }


  "Getting order" when {
    "order exists" should {
      "ruturne status ok " in {
        when(orderService.getOrder(anyString())).thenReturn(Future.successful(Some(Order("1", Nil, None))))
        Get("/order/12235") ~> ordersApi.routes ~> check {
          status shouldBe OK
        }
      }
    }

    "order doesn't exist" should {
      "return not found" in {
        when(orderService.getOrder(anyString())).thenReturn(Future.successful(None))
        Get("/order/12235") ~> ordersApi.routes ~> check {
          status shouldBe NotFound
        }
      }
    }
  }

  "Should be able to add a new dish" in {
    val orderEntry = OrderEntry(None, Dish("1", "Sushi", "", 10.00, "catg_id"), 1)
    when(orderService.addEntry(any(classOf[OrderEntry]), anyString())).thenReturn(Future.successful(1))
    Post("/order/123456/dish", orderEntry) ~> ordersApi.routes ~> check {
      status shouldBe OK
    }
  }

  "Should be able to handle checkout" in {
    Put("/order/123455/checkout") ~> ordersApi.routes ~> check {
      status shouldBe OK
    }
  }
}
