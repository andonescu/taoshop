package it.taoshop.menumodule

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import it.taoshop.configs.{DatabaseConfig, FlywayConfig}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import slick.driver.JdbcProfile
import util.H2ConfigProvider

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by d.stratulat on 12/12/16.
  */
class DishRepositorySpec extends FlatSpec with Matchers with H2ConfigProvider with BeforeAndAfterAll {

  implicit val dbConf: slick.backend.DatabaseConfig[JdbcProfile] = connection
  implicit val exc  = scala.concurrent.ExecutionContext.global
  val driver = slick.driver.PostgresDriver
  val dishRepo = new MenuRepository(driver)
  val flyway = new FlywayConfig() with DatabaseConfig


  override def afterAll(): Unit = {
    flyway.cleanSchema
  }

  override def beforeAll(): Unit = {
    flyway.migrateSchema
  }

  "when interacting with the db" should "insert and retrieve the dish" in {
    val id = UUID.randomUUID().toString
    val newData = DishData(id, "Sushi", "some long description", 1.2, "1", isAvailable = true, Timestamp.valueOf(LocalDateTime.now()))

    Await.result(dishRepo.insertDish(newData), Duration(2, "seconds"))
    Await.result(dishRepo.getDishById(id), Duration(10 ,"seconds")).isDefined  shouldBe true
  }

}
