package it.taoshop.menumodule

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Future
import scala.concurrent.duration._

/**
  * Created by d.stratulat on 12/9/16.
  */

case class MenuData(categories: List[Category])

class MenuSpec extends WordSpec with ScalatestRouteTest with Matchers with MenuFormats with MockitoSugar {

  val timeout = 10 seconds
  val menuRepository: MenuRepository = mock[MenuRepository]
  implicit val exc = scala.concurrent.ExecutionContext.global
  implicit def default(implicit system: ActorSystem) = RouteTestTimeout(4.second)


  "When calling GET dishes" should {
    val dishes = Seq(
      DishData(UUID.randomUUID().toString, "Titel1", "description1", 2.00, "1", true, Timestamp.valueOf(LocalDateTime.now())),
      DishData(UUID.randomUUID().toString, "Titel2", "description2", 3.00, "2", true, Timestamp.valueOf(LocalDateTime.now()))
    )
    val categories = Seq(
      CategoryData("1", "Sushi", true, Timestamp.valueOf(LocalDateTime.now())),
      CategoryData("2", "Sashimi", true, Timestamp.valueOf(LocalDateTime.now()))
    )

    val menu = new Menu(menuRepository)
    when(menuRepository.allActiveDishes()).thenReturn(Future.successful(dishes))
    when(menuRepository.findAllActiveCategories()).thenReturn(Future.successful(categories))

    "should retrieve all the dishes" in {
      Get("/menu/dishes") ~> menu.routes ~> check {
          status shouldBe OK
          val response = responseAs[List[Category]]
        response.find(_.name == "Sushi").get.dishes.nonEmpty shouldBe true
      }
    }

    "should return empty list if categories is empty" in {
      when(menuRepository.findAllActiveCategories()).thenReturn(Future.successful(Seq[CategoryData]()))

      Get("/menu/dishes") ~> menu.routes ~> check {
        status shouldBe OK
        val response = responseAs[List[Category]]
        response.isEmpty shouldBe true
      }
    }

    "should return not found" in {
      when(menuRepository.allActiveDishes).thenReturn(Future.failed(new Throwable))
      Get("/menu/dishes") ~> menu.routes ~> check {
        status shouldBe InternalServerError
      }
    }
  }
}
