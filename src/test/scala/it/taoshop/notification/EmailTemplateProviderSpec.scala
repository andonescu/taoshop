package it.taoshop.notification

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by d.stratulat on 2/6/17.
  */
class EmailTemplateProviderSpec extends FlatSpec with Matchers {
  var subject = new EmailTemplateProvider {}
  "When calling for a template " should "return the html file as astring " in {
    subject.getTemplate(Templates.OrderConfirmationEmail(), Map("name" -> "Dragos")) shouldEqual "<p>Order confirmation email</p>"
  }

  "When calling for kitchen notification email" should "return converted template to string" in {
    subject.getTemplate(Templates.KitchenNotificationEmail(), Map("name" -> "Dragos")) shouldEqual "<p>Kitchen confirmation email</p>"
  }

}
