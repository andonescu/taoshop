name := "taoshop"

version := "1.0"

scalaVersion := "2.12.3"

val akkaHttpVersion = "10.0.9"

val slickVersion = "3.2.1"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,

  "org.slf4j" % "slf4j-nop" % "1.7.22",

  "org.postgresql" % "postgresql" % "9.4.1212.jre7",
  "org.flywaydb" % "flyway-core" % "4.0.3",
  "com.typesafe.slick" %% "slick" % slickVersion,
  "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
  "com.github.jurajburian" %% "mailer" % "1.2.1" withSources,

  "org.typelevel" %% "cats" % "0.9.0",
  "org.freemarker" % "freemarker" % "2.3.23",

  "com.h2database" % "h2" % "1.4.187" % "test",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "org.mockito" % "mockito-core" % "2.4.2" % "test"
)

libraryDependencies ++= (scalaBinaryVersion.value match {
  case "2.10" =>
    compilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full) :: Nil
  case _ =>
    Nil
})

resolvers += Resolver.sonatypeRepo("releases")

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.3")

// if your project uses multiple Scala versions, use this for cross building
addCompilerPlugin("org.spire-math" % "kind-projector" % "0.9.3" cross CrossVersion.binary)



