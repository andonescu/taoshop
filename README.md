# README #


This is the backend app for the taoshop. 
It uses scala and akka-http, it stores data into a postgres data base. 

You will be needing sbt and scala on your machine.

Download scala : https://www.scala-lang.org/download/
Donwload sbt : http://www.scala-sbt.org/
Download postgresql :  https://www.postgresql.org/download/

If you are using OSX then you can use homebrew to install all depencies. 
Install homebrew : https://brew.sh/ 


### What is this repository for? ###

Before you run the app make sure that you  have created a database called taoshop with a user tony with password sushi. 
If all is setup open a terminal window in the root directory of the project and run sbt run. 
